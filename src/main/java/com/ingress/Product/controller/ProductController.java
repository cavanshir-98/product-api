package com.ingress.Product.controller;

import com.ingress.Product.dto.ProductDto;
import com.ingress.Product.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final StudentService service;


    @GetMapping("/list")
    public List<ProductDto> getAllProducts() {
        return service.getAllProducts();
    }


    @PostMapping
    public ProductDto createProduct(@RequestBody ProductDto productDto) {

        return service.createProduct(productDto);
    }

    @GetMapping
    public List<ProductDto> getAllProductsWithPriceOver(@RequestParam Double price) {
        return service.getAllProductsWithPriceOver(price);

    }

    @GetMapping("/category")
    public Set<ProductDto> getAllProductsWithCategory(@RequestParam String name) {
        return service.getAllProductsWithCategory(name);

    }

}
