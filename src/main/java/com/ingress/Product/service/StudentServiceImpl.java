package com.ingress.Product.service;

import com.ingress.Product.dto.CategoryDto;
import com.ingress.Product.dto.ManufacturerDto;
import com.ingress.Product.dto.ProductDto;
import com.ingress.Product.mapper.ManufacturerMapper;
import com.ingress.Product.mapper.ProductMapper;
import com.ingress.Product.model.Category;
import com.ingress.Product.model.Manufacturer;
import com.ingress.Product.model.Product;
import com.ingress.Product.repository.CategoryRepo;
import com.ingress.Product.repository.ProductsRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final ProductsRepo productsRepo;
    private final ModelMapper mapper;
    private final CategoryRepo categoryRepo;
    private final Product product;
    private final ProductDto productDto;
    private final ProductMapper productMapper;
    private final ManufacturerMapper manufacturerMapper;

    @Override
    public List<ProductDto> getAllProducts() {
        List<Product> all = productsRepo.findAll();
        List<ProductDto> productDtoList = all.stream().map(findAll -> {
            ProductDto productDto = productMapper.productToMapDto(findAll);
            ManufacturerDto manufacturerDto = manufacturerMapper.manufacturerToDto(findAll.getManufacturers());
            productDto.setManufacturerDto(manufacturerDto);
            Set<CategoryDto> categoryDtoSet = findAll.getCategories().stream().map(category -> {
                CategoryDto categoryDto = mapper.map(category, CategoryDto.class);
                return categoryDto;
            }).collect(Collectors.toSet());
            productDto.setCategoryDtoSet(categoryDtoSet);
            return productDto;
        }).collect(Collectors.toList());
        return productDtoList;

    }

    @Override
    public ProductDto createProduct(ProductDto productDto) {
        Product product = mapper.map(productDto, Product.class);
        Manufacturer manufacturer = mapper.map(productDto.getManufacturerDto(), Manufacturer.class);
        product.setManufacturers(manufacturer);
        Set<Category> collect = productDto.getCategoryDtoSet().stream().map(categoryDto ->
                categoryRepo.findByName(categoryDto.getName()).orElse(Category.builder()
                        .name(categoryDto.getName()).
                        build())
        ).collect(Collectors.toSet());
        product.setCategories(collect);
        productsRepo.save(product);
        return productDto;

    }

    @Override
    public List<ProductDto> getAllProductsWithPriceOver(Double price) {
        List<Product> products = productsRepo.findAllByPriceGreaterThanEqual(price);
        List<ProductDto> collect = products.stream().map(priceGreaterThan -> {
            ProductDto productDto = mapper.map(priceGreaterThan, ProductDto.class);
            ManufacturerDto manufacturerDto = mapper.map(priceGreaterThan.getManufacturers(), ManufacturerDto.class);
            productDto.setManufacturerDto(manufacturerDto);
            Set<CategoryDto> collect5 = priceGreaterThan.getCategories().stream().map(category -> {
                CategoryDto categoryDto = mapper.map(category, CategoryDto.class);
                return categoryDto;
            }).collect(Collectors.toSet());
            productDto.setCategoryDtoSet(collect5);
            return productDto;
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public Set<ProductDto> getAllProductsWithCategory(String name) {
        Set<Product> products = productsRepo.findAllByCategoriesNameEquals(name);
        Set<ProductDto> collect = products.stream().map(category -> {
            ProductDto productDto = mapper.map(category, ProductDto.class);
            ManufacturerDto manufacturerDto = mapper.map(category.getManufacturers(), ManufacturerDto.class);
            productDto.setManufacturerDto(manufacturerDto);
            Set<CategoryDto> collect5 = category.getCategories().stream().map(product1 -> {
                CategoryDto categoryDto = mapper.map(product1, CategoryDto.class);
                return categoryDto;
            }).collect(Collectors.toSet());
            productDto.setCategoryDtoSet(collect5);
            return productDto;
        }).collect(Collectors.toSet());
        return collect;
    }

}






