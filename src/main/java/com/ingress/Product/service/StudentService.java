package com.ingress.Product.service;

import com.ingress.Product.dto.ProductDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface StudentService {



    List<ProductDto> getAllProducts();

    ProductDto createProduct(ProductDto productDto);

    List<ProductDto> getAllProductsWithPriceOver(Double price);

    Set<ProductDto> getAllProductsWithCategory(String name);
}
