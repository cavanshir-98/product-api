package com.ingress.Product.mapper;

import com.ingress.Product.dto.ManufacturerDto;
import com.ingress.Product.model.Manufacturer;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ManufacturerMapper {


    Manufacturer dtoToEntity(ManufacturerDto manufacturerDto );

    ManufacturerDto manufacturerToDto(Manufacturer manufacturer);
}
