package com.ingress.Product.mapper;


import com.ingress.Product.dto.CategoryDto;
import com.ingress.Product.dto.ProductDto;
import com.ingress.Product.model.Category;
import com.ingress.Product.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.util.Locale;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapper {


    Product dtoToEntity(ProductDto productDto);

    @Mapping(target = "name", source = "product", qualifiedByName = "setName")
    ProductDto productToMapDto(Product product);

    @Named("setName")
    default String setName(Product product) {

        return product.getName().toUpperCase();
    }


}
