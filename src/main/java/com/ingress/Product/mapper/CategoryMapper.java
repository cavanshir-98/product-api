package com.ingress.Product.mapper;

import com.ingress.Product.dto.CategoryDto;
import com.ingress.Product.model.Category;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CategoryMapper {

    CategoryDto categoryToMapDto(Category category);


}
