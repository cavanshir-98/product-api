package com.ingress.Product;

import com.ingress.Product.model.Course;
import com.ingress.Product.model.Student;
import com.ingress.Product.repository.CourseRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
public class ProductApplication implements CommandLineRunner {

    private final CourseRepo courseRepo;

    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        Course byId = courseRepo.getById(1L);
        System.out.println(byId);


//        Course course = new Course();
//        course.setName("MicroService");
//
//        List<Student> list = new ArrayList<>();
//        for (int i = 0; i < 50; i++) {
//            list.add(Student.builder()
//                    .name("Cavansir" + i)
//                    .course(course)
//                    .build());
//        }
//        course.setStudents(list);
//        courseRepo.save(course);
    }
}
