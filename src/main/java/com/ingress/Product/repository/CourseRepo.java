package com.ingress.Product.repository;

import com.ingress.Product.model.Category;
import com.ingress.Product.model.CategoryType;
import com.ingress.Product.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CourseRepo extends JpaRepository<Course,Long> {
    Optional<Category> findByName(CategoryType categoryType);


}
