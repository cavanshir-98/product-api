package com.ingress.Product.repository;

import com.ingress.Product.model.Category;
import com.ingress.Product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface ProductsRepo extends JpaRepository<Product, Long> {

    Set<Product> findAllByCategoriesNameEquals(String name);

    List<Product> findAllByPriceGreaterThanEqual(Double price);

    @Query(value = "select * from category c where c.name", nativeQuery = true)
    Set<Category> findByName();


}
