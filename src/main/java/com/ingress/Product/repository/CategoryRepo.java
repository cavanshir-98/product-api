package com.ingress.Product.repository;

import com.ingress.Product.model.Category;
import com.ingress.Product.model.CategoryType;
import com.ingress.Product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CategoryRepo extends JpaRepository<Category,Long> {
    Optional<Category> findByName(CategoryType categoryType);


}
