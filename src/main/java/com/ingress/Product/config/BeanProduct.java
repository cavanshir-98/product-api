package com.ingress.Product.config;

import com.ingress.Product.model.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanProduct {

    @Bean
    public Product product() {
        return new Product();
    }
}
