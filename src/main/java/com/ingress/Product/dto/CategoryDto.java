package com.ingress.Product.dto;

import com.ingress.Product.model.CategoryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto {


    private Long id;

    private CategoryType name;

}
